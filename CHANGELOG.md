# Furbo Renderer Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.010 - 2020-10-26
### Added
- UPdate to new composer
